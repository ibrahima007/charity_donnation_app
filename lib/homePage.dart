import 'package:charity_Donation_App/screens/explore.dart';
import 'package:charity_Donation_App/screens/heart.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  List<Widget> _children = [
    Heart(),
    Explore(),
    Container(),
    Container(),
    Container(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.purple,
        elevation: 0,
        iconSize: 32,
        items: [
          BottomNavigationBarItem(
            icon: Icon(LineAwesomeIcons.heart_o),
            // ignore: deprecated_member_use
            title: Text("Heart"),
          ),
          BottomNavigationBarItem(
            icon: Icon(LineAwesomeIcons.search),
            // ignore: deprecated_member_use
            title: Text("Search"),
          ),
          BottomNavigationBarItem(
            icon: Icon(LineAwesomeIcons.signal),
            // ignore: deprecated_member_use
            title: Text("Signal"),
          ),
          BottomNavigationBarItem(
            icon: Icon(LineAwesomeIcons.bell_o),
            // ignore: deprecated_member_use
            title: Text("Notification"),
          ),
          BottomNavigationBarItem(
            icon: Icon(LineAwesomeIcons.user),
            // ignore: deprecated_member_use
            title: Text("Profile"),
          ),
        ],
      ),
      body: _children[_currentIndex],
    );
  }
}
